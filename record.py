import os
import cv2
import time
import argparse
import shutil


##create parser
parser = argparse.ArgumentParser()
parser.add_argument("duration", help = "Duration of each video")
parser.add_argument("path", help = "target path for vidoe record")
parser.add_argument("--ipcam", help = "ip of camera", default = 0)
parser.add_argument('--show', help = "show preview", action="store_true")
args = parser.parse_args()


##video duration specified by user
video_duration = int(args.duration) #secs
path = args.path
if path[-1] != '/':
    path = path + '/'

##make cache dir
if not os.path.isdir(path + "cache"):
    os.mkdir(f"{path}cache")
else:
    shutil.rmtree(path + "cache")
    os.mkdir(f"{path}cache")

print(f"save video every {args.duration} seconds.", flush=True)
print(f"video save at {path}", flush=True)

##set capture config
ipcam = args.ipcam
cam = cv2.VideoCapture(ipcam)
fourcc = cv2.VideoWriter_fourcc(*'mp4v')
frame_width = int(cam.get(cv2.CAP_PROP_FRAME_WIDTH))
frame_height = int(cam.get(cv2.CAP_PROP_FRAME_HEIGHT))

##set start time and file name
date = time.localtime()
file_name = time.strftime("%Y%m%d-%H%M%S", date) + ".mp4"
video = cv2.VideoWriter(f"{path}cache/{file_name}", fourcc, 30, (frame_width, frame_height))
start_time = time.time()
video_start_time = start_time
##start capture camera stream and record
print("start recording", flush=True)
while True:
    ret, img = cam.read()
    if args.show:    
        cv2.imshow('test', img)
    video.write(img)
    

    ##show time passed
    current_time = time.time()
    secs_passed = current_time - start_time
    #print(f"\rTime passed: {str(int(secs_passed / 3600)).zfill(2)} : {str(int((secs_passed % 3600) / 60)).zfill(2)} : {str(int(secs_passed % 60)).zfill(2)}", end = "")
    ##sys.stdout.flush()
    ##start record a new video when specified duration is reached
    if current_time - video_start_time >= video_duration:
        video.release()
        os.rename(f"{path}cache/{file_name}", f"{path}{file_name}")
        print(f"Time passed: {str(int(secs_passed / 3600)).zfill(2)} : {str(int((secs_passed % 3600) / 60)).zfill(2)} : {str(int(secs_passed % 60)).zfill(2)}", end = "")
        print(f" ---- {file_name} saved!", flush=True)
        date = time.localtime()
        file_name = time.strftime("%Y%m%d-%H%M%S", date) + ".mp4"
        video = cv2.VideoWriter(f"{path}cache/{file_name}", fourcc, 30, (frame_width, frame_height))
        video_start_time = time.time()

   
    ##esc to exit/
    if 0xFF & cv2.waitKey(5) == 27: 
        print("\nESC pressed", flush=True)
        break
    ##exit when capture failed
    if ret == False: 
        print("\ncapture failed", flush=True)
        break

print(f" ---- {file_name} saved!", flush=True)
print("Exit!", flush=True)


cam.release()
video.release()
cv2.destroyAllWindows()

