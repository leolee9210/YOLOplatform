import cv2 as CV2
import os
import time
import argparse

from numpy import true_divide

parser = argparse.ArgumentParser()
parser.add_argument("input_video", help = "input path of video")
parser.add_argument("output_dir", help = "output path of images")
parser.add_argument("fps", help = "How many frame extracted every second")
args = parser.parse_args()



output_path=args.output_dir  #設定輸出路徑
video_input=args.input_video #設定要處理的影片

print("start cut", flush=True)
def ExtractVideoFrame(video_input,output_path):
    # 輸出資料夾不存在，則建立輸出資料夾
    if not os.path.exists(output_path):
       os.mkdir(output_path)
    times = 0               # 用來記錄幀
    frame_frequency = int(30/(int(args.fps)))    # 提取影片的頻率，每frameFrequency幀提取一張圖片，提取完整影片設置為1
    count = 0               # 記數用，分割的圖片按照count來命名
    cap = CV2.VideoCapture(video_input)  # 讀取影片檔案

    print('開始提取', flush=True)
    while True:
        times += 1
        res, image = cap.read()          # 讀出圖片。res表示是否讀取到圖片，image表示讀取到的每一帧圖片
        if not res:
            print('圖片提取结束', flush=True)
            break
        if times % frame_frequency == 0:
            # picture_gray = CV2.cvtColor(image, CV2.COLOR_BGR2GRAY)  # 將圖片轉成灰度圖
            # image_resize = CV2.resize(image, (368, 640))            # 修改圖片的大小
            img_name = str(count).zfill(6)+'.jpg'
            CV2.imwrite(output_path + os.sep + img_name, image)
            count += 1

            print(f"{img_name} saved!", flush=True)  # 輸出提示
    cap.release() 
ExtractVideoFrame(video_input,output_path)