# 伺服器端使用說明

## 需求環境
* python (https://docs.aws.amazon.com/zh_tw/elasticbeanstalk/latest/dg/eb-cli3-install-windows.html)
    * opencv (終端機內輸入 pip install opencv-python)
* node js
* npm 

### 第一次使用 (使用終端機)

1. **請確認電腦有安裝 Node.js 及 npm**<br>
(安裝及確認：https://ithelp.ithome.com.tw/articles/10157347)

2. **複製此專案檔案**
    ```
    git clone https://gitlab.com/leolee9210/YOLOplatform.git
    ```

3. **cd 至此專案路徑**
    ```
    cd YOLOplatform
    ```

4. **初始化環境**
    ```
    npm i
    ```

5. **運行 index.js**
    ```
    node index.js
    ```
**若成功會出現以下結果**
    ```
    current path: c:\server\YOLOplatform
    folder "class" created
    please create new folder(s) with any wanted class name(s) in folder "class"
    ```
    **專案內會出現資料夾 class**
    ![image.png](./imgforreadme/image1.png)

6. **現在使用瀏覽器連線至https://{伺服器IP或網域名稱}:8888 便會顯示介面**
    ![image.png](./imgforreadme/image3.png)

### 設定環境

#### 增加 class

1. **在資料夾 class 內 新增資料夾並取名為「該班級名稱」**
    ![image.png](./imgforreadme/image2.png)

2. **在該資料夾內放入班級名單(須為xlsx格式)**
    ![image.png](./imgforreadme/image4.png)
    * 儲存名字行的第一欄須為「name」
        ![image.png](./imgforreadme/image5.png)

3. **完成後啟動伺服器便會在介面上看到結果**
    * 增加的 class
        ![image.png](./imgforreadme/image6.png)
    * 增加的名單
        ![image.png](./imgforreadme/image7.png)

#### 設定動作種類

1. **開啟專案資料夾內的 config.json**
    ![image.png](./imgforreadme/image8.png)

2. **"movements" 內可自由設定動作數量及名稱**
    ![image.png](./imgforreadme/image9.png)

3. **設定好後在介面上可以看到結果 (需重新整理頁面)**
    ![image.png](./imgforreadme/image10.png)

#### 設定 ipcam

1. **開啟專案資料夾內的 config.json**
    ![image.png](./imgforreadme/image8.png)

2. **"ipcams" 內可以設定各班級的影像來源**<br>
    *(若沒有設定來源會自動擷取該電腦的webcam)*
    ![image.png](./imgforreadme/image11.png)
    ```
    ## 格式為：
        "ipcams": {
            "班級名稱": "影像來源",
            "example": "rtsp://example",
            "class1": "rtsp://admin:dh123456@10.0.0.202",
            ...
        }
    ```
