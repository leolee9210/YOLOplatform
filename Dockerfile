FROM node:latest
WORKDIR /usr/src/app
RUN apt-get update
RUN apt-get install python3 -y
RUN apt-get install python3-pip -y
RUN pip install opencv-python
COPY . .
RUN npm i
CMD ["node", "index.js"]
