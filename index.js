import express from 'express';
import { urlencoded } from 'express';
import { exec, spawn } from "child_process";
import treeKill from 'tree-kill';
import path from 'path';
import { fileURLToPath, pathToFileURL } from 'url';
import fs, { stat } from 'fs';
import XLSX from 'xlsx';
import os from 'os';

const app = express();
const platform = String(os.platform());
let rawdata = fs.readFileSync('config.json');
let configs = JSON.parse(rawdata);
const port = configs["port"]? configs["port"]: 8888;

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

//console.log(__filename);
console.log(`current path: ${__dirname}`);

//create class folder
if (!fs.existsSync('class')) {
    fs.mkdirSync('class');
    console.log('folder "class" created');
    console.log('please create new folder(s) with any wanted class name(s) in folder "class"');
}


let DB = {}

app.use(urlencoded({ extended: true, limit: "50mb", parameterLimit: 50000 }));
app.use(express.json({limit: "50mb"}));

let getRandomInt = (max) => {
    return Math.floor(Math.random() * max);
}

//post method
//create child process and send command
var cmd;
app.post('/execute', (req, res) => {
    let num = 0;
    while(num == 0 || num in DB){
        num = getRandomInt(10000);
    }
    DB[num] = "";
    console.log(req.body);
    let args = req.body.par.split(',');
    if(args[0] == "record.py"){
        let rawdata = fs.readFileSync('config.json');
        let configs = JSON.parse(rawdata);
        var ipcams = configs["ipcams"];
        if(ipcams[args[2]][req.body.ipcam]){
            //console.log(ipcams[args[2]][req.body.ipcam]);
            args.push('--ipcam')
            args.push(ipcams[args[2]][req.body.ipcam]);
        }
        args[2] = `class/${args[2]}/videos`;
        //create videos folder
        if (!fs.existsSync(args[2])) {
            fs.mkdirSync(args[2]);
        }
        //console.log(args[2]);
    }
    if(args[0] == "cut.py"){
        //args = ["cut.py", class_name, video_name, fps]
        var class_name = args[1];
        var video_name = args[2];
        //create datas folder
        if (!fs.existsSync(`class/${class_name}/datas`)) {
            fs.mkdirSync(`class/${class_name}/datas`);
        }
        //changed args = ["cut.py", input_path, output_path, fps]
        args[1] = `${__dirname}/class/${class_name}/videos/${video_name}`;
        args[2] = `${__dirname}/class/${class_name}/datas/${video_name.split()[0]}`;
        //console.log(`${args[1]}, ${args[2]}`);
    }
    var body_cmd = req.body.cmd;
    if(platform != 'win32'){
        // 'yourrootpassword' | sudo -S ...
        let rawdata = fs.readFileSync('config.json');
        let configs = JSON.parse(rawdata);
        var password = configs["password"];
        if(password){
            if(req.body.cmd == 'python'){
                args.unshift('python3');
            }
            body_cmd = 'echo';
            args.unshift('-S');
            args.unshift('sudo');
            args.unshift('|');
            args.unshift(`'${password}'`);
        }
        else{
            if(req.body.cmd == 'python'){
                body_cmd = 'python3';
            }
        }
    }
    console.log(body_cmd, args);
    //cmd = spawn(body_cmd, args);
    cmd = exec(`${body_cmd} ${args.join(' ')}`, {maxBuffer: 1024 * 1024 * 500});
    cmd.stdout.on('data', (data) => {
        DB[num] += data;
    });
    cmd.stderr.on('error', (mess) => {
        console.log(mess);
        [DB][num] += "process error";
    });
    cmd.on('close', (code) => {
        console.log("Process terminated");
        DB[num] += "\nprocess stopped";
    });

    res.set('Access-Control-Allow-Origin', '*');
    res.send({
        id: num
    });
})

//send saved label data to server
app.post('/label/send',  async (req, res) => {
    //console.log(req.body);
    var data = req.body.data;
    var file_path = `class/${req.body.class}/datas/${req.body.video}`;
    var file_name =  `${file_path}/${String(req.body.id).padStart(6, "0")}.txt`;
    fs.writeFile(file_name, data, function(err) {
        //console.log(file_name  + " saved!");
    });
    res.send();
});

//prepare cfg for train
app.post('/prepareCFGs', (req, res) => {
    var class_name = req.body.class;
    var video_name = req.body.video;
    var class_path = `${__dirname}/class/${class_name}`;
    var video_path = `${class_path}/datas/${video_name}`;
    var cfg_path = `${class_path}/cfg`;
    var template_path = `${__dirname}/template`;
    //create cfg folder
    if (!fs.existsSync(cfg_path)) {
        fs.mkdirSync(cfg_path);
    }
    //create obj.names
    var names = req.body.attr_list;
    var names_text = '';
    for(var i = 0; i < names.length; i++){
        names_text += names[i];
        if(i != names.length - 1)
            names_text += '\n';
    }
    fs.writeFile(`${cfg_path}/obj.names`, names_text, () => {});
    //create obj.datas
    var data = fs.readFileSync(`${template_path}/obj.data`, {encoding:'utf8', flag:'r'});
    data = data.replaceAll('CLASS', String(names.length));
    data = data.replaceAll('TRAIN_PATH', `${cfg_path}/train.txt`);
    data = data.replaceAll('VALID_PATH', `${cfg_path}/valid.txt`);
    data = data.replaceAll('NAMES_PATH', `${cfg_path}/obj.names`);
    data = data.replaceAll('WEIGHT_BACKUP_PATH', class_path);
    data = data.replaceAll('\\', '/');
    fs.writeFile(`${cfg_path}/obj.data`, data, () => {});
    //create yolov4-tiny.cfg
    var cfg = fs.readFileSync(`${template_path}/yolov4-tiny.cfg`, {encoding:'utf8', flag:'r'});
    cfg = cfg.replaceAll('FILTERS', String((names.length + 5) * 3));
    cfg = cfg.replaceAll('CLASS', String(names.length));
    fs.writeFile(`${cfg_path}/yolov4-tiny.cfg`, cfg, () => {});
    //create train.txt & valid.txt
    var train_list = [];
    var valid_list = [];
    var temps = fs.readdirSync(video_path);
    temps.forEach((item) => {
        if(item.split('.')[1] == 'jpg'){
            if(Math.random() < 0.7){
                train_list.push(`${video_path}/${item}`);
            }
            else{
                valid_list.push(`${video_path}/${item}`);
            }
        }
    })
    fs.writeFile(`${cfg_path}/train.txt`, train_list.join('\n').replaceAll('\\', '/'), () => {});
    fs.writeFile(`${cfg_path}/valid.txt`, valid_list.join('\n').replaceAll('\\', '/'), () => {});
})

//start train
app.post('/train', (req, res) => {
    var class_name = req.body.class;
    //sudo ./darknet detector train cfg/obj.data cfg/yolov4-tiny.cfg yolov3-tiny.weights -dont_show
    var class_path = `${__dirname}/class/${class_name}`;
    var cfg_path = `${class_path}/cfg`;
    let rawdata = fs.readFileSync('config.json');
    let configs = JSON.parse(rawdata);
    var darknet_path = configs["darknet"]? `${configs["darknet"]}/`: 'darknet/';
    var cm;
    var arg = `detector train ${cfg_path}/obj.data ${cfg_path}/yolov4-tiny.cfg ${__dirname}/template/yolov4-tiny.conv.29 -dont_show`;
    console.log(arg);
    var args = arg.split(' ');
    //console.log(args);
    if(fs.existsSync(`${class_path}/yolov4-tiny_last.weights`)){
        args[4] = `${class_path}/yolov4-tiny_last.weights`;
    }
    if(platform != 'win32'){
        // 'yourrootpassword' | sudo -S ...
        let rawdata = fs.readFileSync('config.json');
        let configs = JSON.parse(rawdata);
        var password = configs["password"];
        if(password){
            args.unshift(`${darknet_path}darknet`);
            cm = 'echo';
            args.unshift('-S');
            args.unshift('sudo');
            args.unshift('|');
            args.unshift(`'${password}'`);
        }
        else{
            cm = `${darknet_path}darknet`;
        }
    }
    else{
        cm = `${darknet_path}darknet.exe`;
    }
    let num = 0;
    while(num == 0 || num in DB){
        num = getRandomInt(10000);
    }
    DB[num] = "";
    //console.log(`${cm} ${args.join(' ')}`);
    cmd = exec(`${cm} ${args.join(' ')}`, {maxBuffer: 1024 * 1024 * 500});
    //cmd = spawn(cm, args, {stdio: 'inherit'});
    cmd.stdout.on('data', (data) => {
        DB[num] += data;
        /*var temp = data.toString().match(/\d+:/);
        if(temp){
            if(parseInt(temp[0].replace(':', '')) % 100 == 1){
                if(is_start){
                    if(os.platform() === 'win32'){
                        exec('taskkill /pid ' + cmd.pid + ' /T /F');
                    }else{
                        process.kill(-cmd.pid, 'SIGTERM');
                    }
                }
                else{
                    is_start = true;
                }
            }
        }*/
    });
    cmd.stderr.on('error', (mess) => {
        console.log(mess);
        [DB][num] += "\nprocess error";
    });
    cmd.on('exit', (code) => {
        console.log(`exit code: ${code}`);
        DB[num] += "\nprocess stopped";
    });
    res.set('Access-Control-Allow-Origin', '*');
    res.send({
        id: num
    });
})  

//start detect video
app.post('/detect', (req, res) => {
    var class_name = req.body.class;
    var video_name = req.body.video;
    var class_path = `${__dirname}/class/${class_name}`;
    if(!fs.existsSync(`${class_path}/yolov4-tiny_last.weights`)){
        console.log(`no trained weight exists at ${class_path}, please train a weight for this class first`);
        res.send({id: false});
        return 0;
    }
    var cfg_path = `${class_path}/cfg`;
    let rawdata = fs.readFileSync('config.json');
    let configs = JSON.parse(rawdata);
    var output_path = `${class_path}/result`;
    if(!fs.existsSync(output_path)){
        fs.mkdirSync(output_path);
    }
    var darknet_path = configs["darknet"]? `${configs["darknet"]}/`: 'darknet/';
    var cm;
    var arg = `detector demo ${cfg_path}/obj.data ${cfg_path}/yolov4-tiny.cfg ${class_path}/yolov4-tiny_last.weights -dont_show -ext_output ${class_path}/videos/${video_name} > ${output_path}/${video_name}.txt`;
    if(platform != 'win32'){
        cm = `${darknet_path}darknet`;
    }
    else{
        cm = `${darknet_path}darknet.exe`;
    }
    let num = 0;
    while(num == 0 || num in DB){
        num = getRandomInt(10000);
    }
    DB[num] = "";
    console.log(`${cm} ${arg}`);
    cmd = exec(`${cm} ${arg}`, {maxBuffer: 1024 * 1024 * 500});
    cmd.stdout.on('data', (data) => {
        DB[num] += data;
    });
    cmd.stderr.on('error', (mess) => {
        console.log(mess);
        [DB][num] += "\nprocess error";
    });
    cmd.on('exit', (code) => {
        console.log(`exit code: ${code}`);
        if(code == 0){
            DB[num] += `detect result saved`;    
        }
        DB[num] += "\nprocess stopped";
    });
    res.set('Access-Control-Allow-Origin', '*');
    res.send({
        id: num
    });
})


//get method
//request for yolomaster,html
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '/yolomaster.html'));
});


//ipcam list
app.get('/ipcam/:class', (req, res) => {
    let rawdata = fs.readFileSync('config.json');
    let configs = JSON.parse(rawdata);
    var ipcams = configs["ipcams"];
    if(ipcams[req.params.class]){
        res.send(ipcams[req.params.class]);
    }
    else{
        res.send(false);
    }
});


//test 2
app.get('/index', (req, res) => {
    res.sendFile(path.join(__dirname, '/indextest.html'));
});


//test 2
app.get('/image', (req, res) => {
    res.sendFile(path.join(__dirname, '/imagetest.html'));
});

//class list
app.get('/class', (req, res) => {
    var class_list = [];
    var path = `${__dirname}/class`;
    //create class folder
    if (!fs.existsSync(path)) {
        fs.mkdirSync(path);
    }
    let files = fs.readdirSync(path);
    files.forEach(function (item, index){
        class_list.push(item)
    });
    res.send(class_list);
});

//get video list of a class
app.get('/index/:class', (req, res) => {
    var jsonFiles = [];
    var path = `class/${req.params.class}/videos`;
    if(!fs.existsSync(path)){
        fs.mkdirSync(path);
    }
    //console.log(path);
    let files = fs.readdirSync(path);
        files.forEach(function (item, index) {
            if(item != "cache"){
                jsonFiles.push(item);
            }
       }
    );
    //console.log(jsonFiles);
    res.send(jsonFiles);
});

//get image list of a class
app.get('/image/:class/:videoname', (req, res) => {
    var jsonFiles = [];
    var path = `class/${req.params.class}/datas/${req.params.videoname}`;
    //console.log(path);
    let files = fs.readdirSync(path);
    files.forEach(function (item, index) {
        if(item.split(".")[1] == "jpg"){
            jsonFiles.push(item);
        }
    });
    //console.log(jsonFiles);
    res.send(jsonFiles);
});

//get image
app.get('/image/:class/:videoname/:imagename', (req, res) => {
    var img = `${__dirname}/class/${req.params.class}/datas/${req.params.videoname}/${req.params.imagename}`;
    res.sendFile(img);
});

//get name list
app.get('/list/:class', (req, res) => {
    var dir_path = `${__dirname}/class/${req.params.class}`;
    var file_list = fs.readdirSync(dir_path);
    var file_path;
    for(var i = 0; i < file_list.length; i++){
        if(file_list[i].includes('.xlsx')){
            file_path = `${dir_path}/${file_list[i]}`;
            break;
        }
    }
    
    let rawdata = fs.readFileSync('config.json');
    let configs = JSON.parse(rawdata);
    var movements = configs["movements"]? configs["movements"]: [];
    var list = [];
    if(file_path){
        var workbook = XLSX.readFile(file_path)
        var sheet = workbook.Sheets[workbook.SheetNames[0]]
        var data = XLSX.utils.sheet_to_json(sheet)
        for(let i in data){
            list.push(data[i]["name"])
        }
    }
    res.send([movements, list])
})

//get region data
app.get('/data/:class/:videoname', (req, res) => {
    var file_path = `${__dirname}/class/${req.params.class}/datas/${req.params.videoname}`;
    try{
        let files = fs.readdirSync(file_path);
        var all_data = []
        files.forEach(function (item, index) {
            if(item.split(".")[1] == "txt"){
                var data = fs.readFileSync(`${file_path}/${item}`, 'utf8');
                all_data.push(data);
                //console.log(data);
            }
        });
        res.send(all_data);
    }
    catch{
        res.send([])
    }
    
})

//get console log of shell
app.get('/:id', (req, res) => {
    //console.log(req.params);
    res.set('Access-Control-Allow-Origin', '*');
    if(!(req.params.id in DB))
        res.status(404).send();
    res.send(DB[req.params.id]);
    DB[req.params.id] = "";
});


//stop the child process
app.get('/:id/stop', (req, res) => {
    console.log("Kill process");
    treeKill(cmd.pid);
    /*
    if(os.platform() === 'win32'){
        exec('taskkill /pid ' + cmd.pid + ' /T /F');
    }else{
        process.kill(cmd.pid + 1, 'SIGTERM')
    }
    */
    res.send('ok');
});


//start server at port 8888
app.listen(port, () => {
    console.log(`Server start at port ${port}`);
});
